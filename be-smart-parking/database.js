const express = require("express");
const mysql = require("mysql2");

const conn = mysql.createConnection({
  host: "localhost",
  user: "codeigniter",
  password: "codeigniter2019",
  database: "SISIII2024_89191189",
});

conn.connect((err) => {
  if (err) {
    console.log("ERROR: " + err.message);
    return;
  }
  console.log("Database connected!");
});

let methods = {};

methods.Login = (email) => {
  return new Promise((resolve, reject) => {
    conn.query(
      "SELECT * FROM User WHERE email = ?",
      email,
      (err, res, fields) => {
        if (err) {
          return reject(err);
        }
        return resolve(res);
      }
    );
  });
};

methods.Register = (name, surname, email, password) => {
  return new Promise((resolve, reject) => {
    conn.query(
      `INSERT INTO User (name, surname, email, password, admin, e_verified) VALUES (?,?,?,?,0,0)`,
      [name, surname, email, password],
      (err, res) => {
        if (err) {
          return reject(err);
        }
        return resolve(res);
      }
    );
  });
};

methods.GetReservations = (u_id) => {
  return new Promise((resolve, reject) => {
    conn.query(`SELECT * FROM Reservation WHERE u_id = ?`, u_id, (err, res) => {
      if (err) {
        return reject(err);
      }
      return resolve(res);
    });
  });
};

methods.GetSpots = () => {
  return new Promise((resolve, reject) => {
    conn.query(`SELECT * FROM Parking_spot`, (err, res) => {
      if (err) {
        return reject(err);
      }
      return resolve(res);
    });
  });
};

methods.ReservePs = (start, end, date, u_id, ps_id) => {
  const startDatetime = new Date(start)
    .toISOString()
    .slice(0, 19)
    .replace("T", " ");
  const endDatetime = new Date(end)
    .toISOString()
    .slice(0, 19)
    .replace("T", " ");

  const formattedDate = new Date(date).toISOString().split("T")[0];

  return new Promise((resolve, reject) => {
    conn.query(
      `INSERT INTO Reservation (start, end, date, u_id, ps_id) VALUES (?,?,?,?,?)`,
      [startDatetime, endDatetime, formattedDate, u_id, ps_id],
      (err, res) => {
        if (err) {
          return reject(err);
        }
        return resolve(res);
      }
    );
  });
};

methods.Pay = (total, u_id, r_id) => {
  return new Promise((resolve, reject) => {
    conn.query(
      `INSERT INTO Payment (ammount, u_id, r_id) VALUES (?,?,?)`,
      [total, u_id, r_id],
      (err, res) => {
        if (err) {
          return reject(err);
        }
        return resolve(res);
      }
    );
  });
};

methods.Rate = (value, u_id, ps_id) => {
  return new Promise((resolve, reject) => {
    conn.query(
      `INSERT INTO Rating (value, u_id, ps_id) VALUES (?,?,?)`,
      [value, u_id, ps_id],
      (err, res) => {
        if (err) {
          return reject(err);
        }
        return resolve(res);
      }
    );
  });
};


module.exports = methods;
