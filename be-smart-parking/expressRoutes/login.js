const express = require("express");
const login = express.Router();
const database = require("../database");
const bodyParser = require("body-parser");

login.use(bodyParser.json());

login.post("/", async (req, res, next) => {
  try {
    const email = req.body.email;
    const password = req.body.password;
    if (email) {
      const info = await database.Login(email);
      if (info.length > 0) {
        if (password === info[0].password) {
          res.send(info[0]);
        } else {
          res.sendStatus(401);
          console.log("Incorrect credentials");
        }
      } else {
        res.sendStatus(204);
        console.log("User not registered");
      }
    } else {
      res.sendStatus(204);
      console.log("Please enter email and password!");
    }
    res.end();
  } catch (err) {
    console.log(err);
    res.sendStatus(500);
    next();
  }
});

module.exports = login;
