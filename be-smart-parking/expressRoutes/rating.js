const express = require("express");
const rating = express.Router();
const database = require("../database");
const bodyParser = require("body-parser");

rating.use(bodyParser.json());

rating.post("/", async (req, res, next) => {
  try {
    const value = req.body.value;
    const u_id = req.body.u_id;
    const ps_id = req.body.ps_id;
    if (value && u_id && ps_id) {
      await database.Rate(value, u_id, ps_id);
      res.sendStatus(200);
    } else {
      res.sendStatus(401);
    }
    res.end();
  } catch (err) {
    console.log(err);
    res.sendStatus(500);
    next();
  }
});

module.exports = rating;
