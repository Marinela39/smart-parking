const express = require("express");
const register = express.Router();
const database = require("../database");
const bodyParser = require("body-parser");

register.use(bodyParser.json());

register.post("/", async (req, res, next) => {
  try {
    const name = req.body.name;
    const surname = req.body.surname;
    const email = req.body.email;
    const password = req.body.password;
    if (email) {
      await database.Register(name, surname, email, password);
      console.log("Registered!");
    } else {
      res.sendStatus(204);
      console.log("Register error!");
    }
    res.end();
  } catch (err) {
    console.log(err);
    res.sendStatus(500);
    next();
  }
});



module.exports = register;
