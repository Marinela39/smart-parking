const express = require("express");
const reservations = express.Router();
const database = require("../database");
const bodyParser = require("body-parser");

reservations.use(bodyParser.json());

reservations.get("/", async (req, res) => {
  try {
    u_id = req.query.u_id;
    if (u_id) {
      const reservations = await database.GetReservations(u_id);
      if (reservations.length > 0) {
        res.send(reservations);
      } else {
        res.sendStatus(404);
      }
    } else {
      res.sendStatus(400);
    }
  } catch (error) {
    res.sendStatus(500);
  }
});

reservations.get("/getspots", async (req, res) => {
  try {
    const spots = await database.GetSpots();
    if (spots.length > 0) {
      res.send(spots);
    } else {
      res.sendStatus(404);
    }
  } catch (error) {
    res.sendStatus(500);
  }
});

reservations.post("/", async (req, res, next) => {
  try {
    const start = req.body.start;
    const end = req.body.end;
    const date = req.body.date;
    const u_id = req.body.u_id;
    const ps_id = req.body.ps_id;
    if (start && end && date && u_id && ps_id) {
      await database.ReservePs(start, end, date, u_id, ps_id);
      console.log("Reserved!");
    } else {
      res.sendStatus(204);
      console.log("Reservation error!");
    }
    res.end();
  } catch (err) {
    console.log(err);
    res.sendStatus(500);
    next();
  }
});

reservations.post("/pay", async (req, res, next) => {
  try {
    const ammount = req.body.ammount;
    const u_id = req.body.u_id;
    const r_id = req.body.r_id;
    if (ammount && u_id && r_id) {
      await database.Pay(ammount, u_id, r_id);
      res.sendStatus(200);
    } else {
      res.sendStatus(204);
      console.log("Payment error!");
    }
    res.end();
  } catch (err) {
    console.log(err);
    res.sendStatus(500);
    next();
  }
});

module.exports = reservations;
