const express = require("express");
const app = express();
const cors = require("cors");
const login = require("./expressRoutes/login");
const register = require("./expressRoutes/register");
const reservations = require("./expressRoutes/reservations");
const rating = require("./expressRoutes/rating");
const port = 8102;
const path = require("path");

app.use(express.static(path.join(__dirname, "build")))

app.get("/", (req, res) => {
    res.sendFile(path.join(__dirname, "build", "index.html")) 
})


app.use(express.urlencoded({ extended: true }));
app.use(
  cors({
    methods: ["GET", "POST"],
  })
);

app.use("/login", login);
app.use("/register", register);
app.use("/reservations", reservations);
app.use("/rating", rating);

app.listen(port, () => {
  console.log(`Server is running on port: ${port}`);
});
