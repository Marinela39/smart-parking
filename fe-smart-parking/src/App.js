import { BrowserRouter, Routes, Route } from "react-router-dom";
import NavComponent from "./navbar/navComponent";
import HomeComponent from "./home/homeComponent";
import LoginComponent from "./login/loginComponent";
import RegisterComponent from "./register/registerComponent";
import MyaccountComponent from "./myaccount/myaccountComponent";
import ReservationComponent from "./reservation/reservationComponent";
import PaymentComponent from "./payment/paymentComponent";
import { useState } from "react";

export default function App() {
  const [userData, setUserData] = useState({ id: "", name: "", surname: "" });
  const [loggedIn, setLoggedIn] = useState(false);
  const [resInfo, setResInfo] = useState({
    start: "",
    end: "",
    price: 0,
    id: "",
  });

  const handleUserData = (data) => {
    setUserData(data);
  };

  const handleLogin = (status) => {
    setLoggedIn(status);
  };

  const resInfoHandler = (start, end, price, id) => {
    setResInfo(start, end, price, id);
  };

  return (
    <>
      <BrowserRouter>
        <NavComponent loggedIn={loggedIn} />
        <Routes>
          <Route path="/" element={<HomeComponent />} />
          <Route
            path="/loginpage"
            element={
              <LoginComponent
                onUserData={handleUserData}
                onLogin={handleLogin}
              />
            }
          />
          <Route path="/registerpage" element={<RegisterComponent />} />
          <Route
            path="/myaccount"
            element={
              <MyaccountComponent
                userData={userData}
                resInfoHandler={resInfoHandler}
              />
            }
          />
          <Route
            path="/reservations"
            element={<ReservationComponent userId={userData.id} />}
          />
          <Route
            path="/payment"
            element={<PaymentComponent resInfo={resInfo} u_id={userData.id} />}
          />
        </Routes>
      </BrowserRouter>
    </>
  );
}
