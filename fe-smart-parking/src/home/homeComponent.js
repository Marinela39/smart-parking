import React from "react";
import "./homeStyle.css";
import Image1 from "../Images/img1.jpeg";
import Image2 from "../Images/img2.jpeg";
import Image3 from "../Images/img3.jpeg";

export default function HomeComponent() {
  return (
    <>
      <div className="homeDiv">
        <div className="textDiv">
          <div className="homeText">
            <div
              id="carouselExampleIndicators"
              className="carousel slide"
              data-bs-ride="carousel"
            >
              <div className="carousel-indicators">
                <button
                  type="button"
                  data-bs-target="#carouselExampleIndicators"
                  data-bs-slide-to="0"
                  className="active"
                  aria-current="true"
                  aria-label="Slide 1"
                ></button>
                <button
                  type="button"
                  data-bs-target="#carouselExampleIndicators"
                  data-bs-slide-to="1"
                  aria-label="Slide 2"
                ></button>
                <button
                  type="button"
                  data-bs-target="#carouselExampleIndicators"
                  data-bs-slide-to="2"
                  aria-label="Slide 3"
                ></button>
              </div>
              <div
                className="carousel-inner"
                style={{ width: "50vw", height: "60vh" }}
              >
                <div className="carousel-item active" data-bs-interval="2000">
                  <img src={Image1} alt="Image" style={{ width: "100%" }} />
                </div>
                <div className="carousel-item" data-bs-interval="2000">
                  <img src={Image2} style={{ width: "100%" }} alt="Image" />
                </div>
                <div className="carousel-item" data-bs-interval="2000">
                  <img src={Image3} style={{ width: "100%" }} alt="Image" />
                </div>
              </div>
              <button
                className="carousel-control-prev"
                type="button"
                data-bs-target="#carouselExampleIndicators"
                data-bs-slide="prev"
              >
                <span
                  className="carousel-control-prev-icon"
                  aria-hidden="true"
                ></span>
                <span className="visually-hidden">Previous</span>
              </button>
              <button
                className="carousel-control-next"
                type="button"
                data-bs-target="#carouselExampleIndicators"
                data-bs-slide="next"
              >
                <span
                  className="carousel-control-next-icon"
                  aria-hidden="true"
                ></span>
                <span className="visually-hidden">Next</span>
              </button>
            </div>
          </div>
        </div>
        <div className="cards">
          <div className="card">
            <div className="card-header">Customer review</div>
            <div className="card-body">
              <blockquote className="blockquote mb-0">
                <p>The best parking service on the market.</p>
                <footer className="blockquote-footer">Janez Novak</footer>
              </blockquote>
            </div>
          </div>

          <div className="card">
            <div className="card-header">Customer review</div>
            <div className="card-body">
              <blockquote className="blockquote mb-0">
                <p>Easy to use.</p>
                <footer className="blockquote-footer">Marija Miletic</footer>
              </blockquote>
            </div>
          </div>

          <div className="card">
            <div className="card-header">Customer review</div>
            <div className="card-body">
              <blockquote className="blockquote mb-0">
                <p>I recommend it, 5 stars.</p>
                <footer className="blockquote-footer">Luka Petrovic</footer>
              </blockquote>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
