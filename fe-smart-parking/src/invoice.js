import React from "react";
import { Document, Page, Text, View, StyleSheet, Font, Image } from "@react-pdf/renderer";

const Invoice = ({data}) => (

    <Document>
      <Page style={styles.body}>
        <Text style={styles.title}>Smart Parking - Invoice for parking</Text>
        <Text style={styles.subtitle}>
          Parking Id: {data.id}
        </Text>
        <View style={styles.table}>
          <View style={styles.tableRow}>
            <View style={styles.tableColHeader}>
              <Text style={styles.tableCellHeader}>Starting time</Text>
            </View>
            <View style={styles.tableColHeader}>
              <Text style={styles.tableCellHeader}>End time</Text>
            </View>
            <View style={styles.tableColHeader}>
              <Text style={styles.tableCellHeader}>Total in minutes</Text>
            </View>
            <View style={styles.tableColHeader}>
              <Text style={styles.tableCellHeader}>Price per minute</Text>
            </View>
          </View>
  
          <View style={styles.tableRow}>
            <View style={styles.tableCol}>
              <Text style={styles.tableCell}>{data.startTime}</Text>
            </View>
            <View style={styles.tableCol}>
              <Text style={styles.tableCell}>{data.endTime}</Text>
            </View>
            <View style={styles.tableCol}>
              <Text style={styles.tableCell}>{data.differenceInMinutes}</Text>
            </View>
            <View style={styles.tableCol}>
              <Text style={styles.tableCell}>0.0085 €</Text>
            </View>
          </View>
        </View>
        <Text style={styles.text}>Total ammount in euros before tax: {(data.differenceInMinutes*0.0085).toFixed(2)} </Text>
        <Text style={styles.text}>Tax: 15%</Text>
        <Text style={styles.text}>Total ammount in euros after tax: {(data.differenceInMinutes*0.0085 + data.differenceInMinutes*0.0015).toFixed(2)}</Text>
        
        <Text style={styles.subtitle}>
         {new Date().toDateString()}
        </Text>
        <Image
      style={styles.image}
      src="https://cdn.shopify.com/s/files/1/0594/4639/5086/files/underline_b49eefdc-d72c-4e68-84a4-dcbd1404a772.jpg?v=1680536820"
    />
      </Page>
    </Document>
  );
  
  Font.register({
    family: 'Oswald',
    src: 'https://fonts.gstatic.com/s/oswald/v13/Y_TKV6o8WovbUd3m_X9aAA.ttf'
  });
  
  const styles = StyleSheet.create({
    body: {
      paddingTop: 35,
      paddingBottom: 65,
      paddingHorizontal: 35,
    },
    title: {
      fontSize: 24,
      textAlign: 'center',
      fontFamily: 'Oswald'
    },
   
    subtitle: {
      fontSize: 18,
      margin: 12,
      fontFamily: 'Oswald'
    },
    text: {
      margin: 12,
      fontSize: 14,
      textAlign: 'justify',
      fontFamily: 'Times-Roman'
    },
    
    header: {
      fontSize: 12,
      marginBottom: 20,
      textAlign: 'center',
      color: 'grey',
    },
    table: {
      display: "table",
      width: "500px",
      borderStyle: "solid",
      borderWidth: 1,
      borderRightWidth: 0,
      borderBottomWidth: 0
    },
    tableRow: { 
      margin: "auto", 
      flexDirection: "row" 
    },
    tableColHeader: { 
      width: "25%", 
      borderStyle: "solid", 
      borderWidth: 1, 
      borderLeftWidth: 0, 
      borderTopWidth: 0
    },
    tableCol: { 
      width: "25%", 
      borderStyle: "solid", 
      borderWidth: 1, 
      borderLeftWidth: 0, 
      borderTopWidth: 0 
    },
    tableCellHeader: { 
      margin: "auto", 
      margin: 5, 
      fontSize: 12,
      fontWeight: "bold"
    },
    tableCell: { 
      margin: "auto", 
      margin: 5, 
      fontSize: 10 
    },
    image: {
      width: '20%',
      height: '10%',
      marginLeft: '80%',
      marginTop: '-10%'
    },
   
  });



export default Invoice;