import React from "react";
import { Link } from "react-router-dom";
import "./loginStyle.css";
import { useState } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";

export default function LoginComponent(props) {
  const [user, setUser] = useState({});
  const [userData, setUserData] = useState({ id: "", name: "", surname: "" });
  const [error, setError] = useState(false);
  const navigator = useNavigate();

  const getText = (e) => {
    setUser({
      ...user,
      [e.target.name]: e.target.value,
    });
  };

  const postLogin = () => {
    axios
      .post("http://88.200.63.148:8102/login", {
        email: user.email,
        password: user.password,
      })
      .then((response) => {
        if (response.status === 204 || response.status === 401) {
          setError(true);
        } else {
          setError(false);
          props.onUserData(response.data);
          props.onLogin(true);
          navigator("/myaccount");
        }
      })
      .catch((error) => {
        setError(true);
        console.log(error);
      });
  };

  return (
    <>
      <div className="loginContainer">
        <div className="loginBox">
          <h1>Smart Parking - Login</h1>
          {error && <h5 style={{ color: "red" }}>Wrong data, try again!</h5>}
          <form style={{ width: "30vw" }}>
            <div className="row mb-3">
              <label htmlFor="inputEmail3" className="col-sm-2 col-form-label">
                Email
              </label>
              <div className="col-sm-10">
                <input
                  name="email"
                  type="email"
                  className="form-control"
                  id="inputEmail3"
                  onChange={(e) => getText(e)}
                />
              </div>
            </div>
            <div className="row mb-3">
              <label
                htmlFor="inputPassword3"
                className="col-sm-2 col-form-label"
              >
                Password
              </label>
              <div className="col-sm-10">
                <input
                  name="password"
                  type="password"
                  className="form-control"
                  id="inputPassword3"
                  onChange={(e) => getText(e)}
                />
              </div>
            </div>
            <div className="col-sm-10">
              <Link to="/resetpassword">Forgot password?</Link>
            </div>
            <button
              type="button"
              className="btn btn-primary"
              onClick={postLogin}
            >
              Sign in
            </button>
            <hr />
            <h5>Is this your first time?</h5>
            <p>
              You can create a new account <Link to="/registerpage">here.</Link>
            </p>
            <div className="col-sm-13">
              <button
                type="submit"
                className="btn btn-primary"
                style={{ marginLeft: "0" }}
              >
                Contact customer service
              </button>
            </div>
          </form>
        </div>
      </div>
    </>
  );
}
