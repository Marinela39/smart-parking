import React, { useEffect, useState } from "react";
import "./myaccountStyle.css";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import Invoice from "../invoice";
import { pdf } from "@react-pdf/renderer";

export default function MyaccountComponent(props) {
  const userData = props.userData;
  const [user, setUser] = useState(userData);
  const [reservation, setReservation] = useState([]);
  const [ratedSpots, setRatedSpots] = useState([]);
  const navigate = useNavigate();

  const generatePDF = async (start, end, id) => {
    const startTime = new Date(start);
    const endTime = new Date(end);
    const differenceInMinutes = Math.floor((endTime - startTime) / (1000 * 60));

    const blob = await pdf(
      <Invoice
        data={{
          id,
          startTime: startTime.toLocaleTimeString(),
          endTime: endTime.toLocaleTimeString(),
          differenceInMinutes,
        }}
      />
    ).toBlob();
    const url = URL.createObjectURL(blob);
    window.open(url, "_blank");
  };

  const calculateTime = (start, end) => {
    const startTime = new Date(start);
    const endTime = new Date(end);
    const elapsedTime = endTime - startTime;

    const hours = Math.floor(elapsedTime / (1000 * 60 * 60));
    const minutes = Math.floor((elapsedTime % (1000 * 60 * 60)) / (1000 * 60));

    return hours.toString() + "h " + minutes.toString() + "min";
  };

  const GetReservations = () => {
    axios
      .get("http://88.200.63.148:8102/reservations?u_id=" + user.id)
      .then((response) => {
        setReservation(response.data);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  useEffect(() => {
    GetReservations();
  }, []);

  const navigateToPayment = (start, end, id) => {
    const startTime = new Date(start);
    const endTime = new Date(end);
    const differenceInMinutes = Math.floor((endTime - startTime) / (1000 * 60));

    props.resInfoHandler({
      start: new Date(start).toLocaleTimeString(),
      end: new Date(end).toLocaleTimeString(),
      price: 0.1 * differenceInMinutes,
      id: id,
    });
    navigate("/payment");
  };

  const giveRating = (rating, id) => {
    axios
      .post("http://88.200.63.148:8102/rating", {
        u_id: user.id,
        ps_id: id,
        value: rating,
      })
      .then((response) => {
        if (response.status === 200) {
          alert(
            "Rating submitted successfully for parking spot " +
              id +
              ". Rating value: " +
              rating
          );
          setRatedSpots([...ratedSpots, id]);
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  return (
    <>
      <div className="accountWrapper">
        <div className="pageInfo">
          <h2 style={{ fontSize: "2.5vw" }}>Previous reservations</h2>
          <div
            className="resCards"
            style={{ display: "flex", flexWrap: "wrap" }}
          >
            {reservation && reservation.length > 0 ? (
              reservation.map((reservation) => {
                if (
                  new Date(reservation.date).setHours(0, 0, 0, 0) <
                  new Date().setHours(0, 0, 0, 0)
                ) {
                  return (
                    <div
                      className="card"
                      style={{
                        width: "14rem",
                        margin: "1rem",
                        fontSize: "0.8vw",
                      }}
                      key={reservation.id}
                    >
                      <img
                        src="https://img.freepik.com/free-vector/illustrated-appointment-booking-with-smartphone_52683-38829.jpg"
                        className="card-img-top"
                        alt="..."
                      />
                      <div className="card-body">
                        <h5 className="card-title" style={{ fontSize: "1vw" }}>
                          Reservation {reservation.id}
                        </h5>
                        <p className="card-text">
                          You have reserved the parking spot {reservation.ps_id}{" "}
                          on {new Date(reservation.date).toLocaleDateString()}.
                          You kept this reservation from{" "}
                          {new Date(reservation.start).toLocaleTimeString()} to{" "}
                          {new Date(reservation.end).toLocaleTimeString()}. You
                          paid:{" "}
                          {0.01 *
                            Math.floor(
                              (new Date(reservation.end) -
                                new Date(reservation.start)) /
                                (1000 * 60)
                            )}{" "}
                          €
                        </p>
                        <button
                          type="button"
                          className="btn btn-primary"
                          style={{ fontSize: "0.7vw" }}
                        >
                          Reserve again
                        </button>
                        <button
                          onClick={() => {
                            generatePDF(
                              reservation.start,
                              reservation.end,
                              reservation.id
                            );
                          }}
                          className="btn btn-primary"
                          style={{ marginLeft: "0.5vw", fontSize: "0.7vw" }}
                        >
                          Get receipt
                        </button>
                        <div style={{ marginTop: "1vh" }}>
                          Give a rating
                          <input
                            type="radio"
                            value="1"
                            id="rating"
                            name="rating"
                            style={{marginLeft: "1vw"}}
                            disabled={ratedSpots.includes(reservation.ps_id)}
                            onChange={(e) => {
                              giveRating(e.target.value, reservation.ps_id);
                            }}
                          />
                          <input
                            type="radio"
                            value="2"
                            id="rating"
                            name="rating"
                            disabled={ratedSpots.includes(reservation.ps_id)}
                            onChange={(e) => {
                              giveRating(e.target.value, reservation.ps_id);
                            }}
                          />
                          <input
                            type="radio"
                            value="3"
                            id="rating"
                            name="rating"
                            disabled={ratedSpots.includes(reservation.ps_id)}
                            onChange={(e) => {
                              giveRating(e.target.value, reservation.ps_id);
                            }}
                          />
                          <input
                            type="radio"
                            value="4"
                            id="rating"
                            name="rating"
                            disabled={ratedSpots.includes(reservation.ps_id)}
                            onChange={(e) => {
                              giveRating(e.target.value, reservation.ps_id);
                            }}
                          />
                          <input
                            type="radio"
                            value="5"
                            id="rating"
                            name="rating"
                            disabled={ratedSpots.includes(reservation.ps_id)}
                            onChange={(e) => {
                              giveRating(e.target.value, reservation.ps_id);
                            }}
                          />
                        </div>
                      </div>
                    </div>
                  );
                }
                return null;
              })
            ) : (
              <p>No reservations found</p>
            )}
          </div>
        </div>
        <div className="accountInfo">
          {reservation && reservation.length > 0 ? (
            <div
              style={{
                display: "flex",
                flexDirection: "row",
                justifyContent: "center",
                alignItems: "center",
                marginBottom: "2vh",
                marginLeft: "1vw",
              }}
            >
              <i className="fa-solid fa-user-tie fa-2xl"></i>
              <h4 style={{ marginLeft: "0.8vw", fontSize: "1.8vw" }}>
                {user.name} {user.surname}
              </h4>
            </div>
          ) : (
            <div
              style={{
                display: "flex",
                flexDirection: "row",
                justifyContent: "center",
                alignItems: "center",
                marginBottom: "2vh",
                marginLeft: "1vw",
              }}
            >
              <i className="fa-solid fa-user-tie fa-2xl"></i>
              <h4 style={{ marginLeft: "0.8vw", fontSize: "1.8vw" }}>
                New User
              </h4>
            </div>
          )}
          <hr />

          <div>
            <p style={{ marginLeft: "1vw", fontSize: "1.1vw" }}>
              <b>Active reservations</b>
            </p>
            <div className="list-group" style={{ fontSize: "1vw" }}>
              {reservation && reservation.length > 0 ? (
                reservation.map((reservation) => {
                  if (
                    new Date(reservation.date).setHours(0, 0, 0, 0) >=
                    new Date().setHours(0, 0, 0, 0)
                  ) {
                    return (
                      <a
                        href="#"
                        className="list-group-item list-group-item-action "
                        aria-current="true"
                        key={reservation.id}
                      >
                        <div className="d-flex w-100 justify-content-between">
                          <h5 className="mb-1" style={{ fontSize: "1.3vw" }}>
                            Reservation id: {reservation.id}
                          </h5>
                          <small>
                            {new Date(reservation.date).toLocaleDateString()}
                          </small>
                        </div>
                        <p className="mb-1">
                          Start time:{" "}
                          {new Date(reservation.start).toLocaleTimeString()}
                        </p>
                        <p className="mb-1">
                          End time:{" "}
                          {new Date(reservation.end).toLocaleTimeString()}
                        </p>

                        <button
                          type="button"
                          className="btn btn-danger"
                          style={{ fontSize: "1vw" }}
                          onClick={() => {
                            navigateToPayment(
                              reservation.start,
                              reservation.end,
                              reservation.id
                            );
                          }}
                        >
                          Pay now
                        </button>
                      </a>
                    );
                  }
                  return null;
                })
              ) : (
                <p>No reservations found</p>
              )}
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
