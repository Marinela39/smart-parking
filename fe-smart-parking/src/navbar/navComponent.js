import React from "react";
import { Link } from "react-router-dom";
import "./navStyle.css";

export default function NavComponent(props) {
  const loggedIn = props.loggedIn;

  const prices = () => {
    return alert("The price for one minute is 0.01€.");
  };

  return (
    <>
      <nav
        className="navbar navbar-expand navbar-light"
        style={{ backgroundColor: "#e3f2fd" }}
      >
        <div className="container-fluid">
          <i
            className="fa-solid fa-square-parking fa-2xl"
            style={{ color: "#357bb1" }}
          ></i>
          <a
            className="navbar-brand"
            href="#"
            style={{ marginLeft: "2px", fontSize: "1.3vw" }}
          >
            Smart Parking
          </a>
          <button
            className="navbar-toggler"
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navbarNavDropdown"
            aria-controls="navbarNavDropdown"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon"></span>
          </button>
          <div
            className="collapse navbar-collapse"
            id="navbarNavDropdown"
            style={{
              alignItems: "center",
              justifyContent: "center",
              fontSize: "25px",
            }}
          >
            <ul className="navbar-nav" style={{ fontSize: "1.3vw" }}>
              <li className="nav-item">
                <Link
                  to="/"
                  className="nav-link active"
                  aria-current="page"
                  href="#"
                >
                  Home
                </Link>
              </li>
              <li className="nav-item">
                <Link to="/reservations" className="nav-link active" href="#">
                  Make a reservation
                </Link>
              </li>
              <li className="nav-item">
                <a onClick={prices} className="nav-link active" href="#">
                  Pricing
                </a>
              </li>
              {loggedIn ? (
                <li className="nav-item">
                  <Link to="/myaccount" className="nav-link active" href="#">
                    My account
                  </Link>
                </li>
              ) : (
                <li className="nav-item dropdown">
                  <a
                    className="nav-link active dropdown-toggle"
                    href="#"
                    id="navbarDropdownMenuLink"
                    role="button"
                    data-bs-toggle="dropdown"
                    aria-expanded="false"
                  >
                    Account
                  </a>
                  <ul
                    className="dropdown-menu"
                    aria-labelledby="navbarDropdownMenuLink"
                  >
                    <li>
                      <Link to="/loginpage" className="dropdown-item" href="#">
                        Login
                      </Link>
                    </li>
                    <li>
                      <Link
                        to="/registerpage"
                        className="dropdown-item"
                        href="#"
                      >
                        Register
                      </Link>
                    </li>
                  </ul>
                </li>
              )}
            </ul>
          </div>
        </div>
      </nav>
    </>
  );
}
