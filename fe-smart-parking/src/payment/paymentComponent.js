import React, { useState } from "react";
import InputMask from "react-input-mask";
import "./paymentComponentStyle.css"; // Optional: for custom styling
import axios from "axios";
import { useNavigate } from "react-router-dom";

export default function PaymentComponent(props) {
  const [cardNumber, setCardNumber] = useState("");
  const [expiryDate, setExpiryDate] = useState("");
  const [cvv, setCvv] = useState("");
  const resInfo = props.resInfo;
  const u_id = props.u_id;
  const navigator = useNavigate();

  const handleCardNumberChange = (e) => {
    setCardNumber(e.target.value);
  };

  const handleExpiryDateChange = (e) => {
    setExpiryDate(e.target.value);
  };

  const handleCvvChange = (e) => {
    setCvv(e.target.value);
  };

  const postPayment = (e) => {
    axios
      .post("http://88.200.63.148:8102/reservations/pay", {
        u_id: u_id,
        r_id: resInfo.id,
        ammount: resInfo.price,
      })
      .then((response) => {
        if (response.status === 200) {
            alert("Payment successful!");
            navigator("/myaccount");
        }
        else console.log("Payment failed!");
      })
      .catch((error) => {
        console.log(error);
      });
  };

  return (
    <>
      <div className="paymentWrapper">
        <div className="credit-card-form">
          <h2>Pay for your reservation</h2>
          <form>
            <div className="form-group">
              <label htmlFor="cardNumber">Card Number</label>
              <InputMask
                mask="9999 9999 9999 9999"
                value={cardNumber}
                onChange={handleCardNumberChange}
                className="form-control"
                id="cardNumber"
                placeholder="1234 5678 9012 3456"
              />
            </div>
            <div className="form-group">
              <label htmlFor="expiryDate">Expiry Date</label>
              <InputMask
                mask="99/99"
                value={expiryDate}
                onChange={handleExpiryDateChange}
                className="form-control"
                id="expiryDate"
                placeholder="MM/YY"
              />
            </div>
            <div className="form-group">
              <label htmlFor="cvv">CVV</label>
              <InputMask
                mask="999"
                value={cvv}
                onChange={handleCvvChange}
                className="form-control"
                id="cvv"
                placeholder="123"
              />
            </div>
            <div className="form-group">
              <p>Start time: {resInfo.start}</p>
              <p>End time: {resInfo.end}</p>
              <p>Total ammount: {resInfo.price} € </p>
            </div>
            <button
              type="button"
              className="btn btn-primary"
              id="btn"
              onClick={postPayment}
            >
              Submit Payment
            </button>
          </form>
        </div>
      </div>
    </>
  );
}
