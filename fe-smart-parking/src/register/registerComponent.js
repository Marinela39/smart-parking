import React from "react";
import axios from "axios";
import { useState } from "react";
import { useNavigate } from "react-router-dom";
import "./registerStyle.css";

export default function RegisterComponent() {
  const [user, setUser] = useState({});

  const navigate = useNavigate();

  const getText = (e) => {
    setUser({
      ...user,
      [e.target.name]: e.target.value,
    });
  };

  const postRegister = () => {
    axios
      .post("http://88.200.63.148:8102/register", {
        name: user.name,
        surname: user.surname,
        email: user.email,
        password: user.password,
      })
      .then((response) => {
        if (response.status === 200) {
          alert("User registered!");
          navigate("/loginpage");
        } else {
          console.log("User not registered!");
        }
      })
      .catch((err) => {
        console.log("Missing data!");
      });
  };

  return (
    <>
      <div className="loginContainer">
        <div className="loginBox">
          <h1>Create an account</h1>
          <form style={{ width: "30vw" }}>
            <div className="row mb-3">
              <label htmlFor="inputEmail3" className="col-sm-2 col-form-label">
                Name
              </label>
              <div className="col-sm-10">
                <input
                  name="name"
                  type="text"
                  className="form-control"
                  id="inputEmail3"
                  onChange={(e) => getText(e)}
                />
              </div>
            </div>
            <div className="row mb-3">
              <label htmlFor="inputEmail3" className="col-sm-2 col-form-label">
                Surname
              </label>
              <div className="col-sm-10">
                <input
                  name="surname"
                  type="text"
                  className="form-control"
                  id="inputEmail3"
                  onChange={(e) => getText(e)}
                />
              </div>
            </div>
            <div className="row mb-3">
              <label htmlFor="inputEmail3" className="col-sm-2 col-form-label">
                Email
              </label>
              <div className="col-sm-10">
                <input
                  name="email"
                  type="email"
                  className="form-control"
                  id="inputEmail3"
                  onChange={(e) => getText(e)}
                />
              </div>
            </div>
            <div className="row mb-3">
              <label
                htmlFor="inputPassword3"
                className="col-sm-2 col-form-label"
              >
                Password
              </label>
              <div className="col-sm-10">
                <input
                  name="password"
                  type="password"
                  className="form-control"
                  id="inputPassword3"
                  onChange={(e) => getText(e)}
                />
              </div>
            </div>
            <button
              type="button"
              className="btn btn-primary"
              onClick={postRegister}
            >
              Register
            </button>
            <hr />
            <h5>Trouble creating an account?</h5>
            <p>Contact our customer service.</p>
            <div className="col-sm-13">
              <button
                type="submit"
                className="btn btn-primary"
                style={{ marginLeft: "0" }}
              >
                Contact customer service
              </button>
            </div>
          </form>
        </div>
      </div>
    </>
  );
}
