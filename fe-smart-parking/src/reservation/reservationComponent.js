import React, { useState, useEffect } from "react";
import axios from "axios";
import "./reservationStyle.css";
import { useNavigate } from "react-router-dom";

export default function ReservationComponent(props) {
  const [spots, setSpots] = useState([]);
  const [spotData, setSpotData] = useState({});
  const navigator = useNavigate();
  const userId = props.userId;

  useEffect(() => {
    getSpots();
  }, []);

  const getText = (e) => {
    const { name, value } = e.target;
    setSpotData({
      ...spotData,
      [name]: value,
    });
  };

  const getSpots = () => {
    axios
      .get("http://88.200.63.148:8102/reservations/getspots")
      .then((response) => {
        setSpots(response.data);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const reserveSpot = () => {
    let startTimestamp = new Date(
      `${spotData.date}T${spotData.start}`
    ).toISOString();
    let endTimestamp = new Date(
      `${spotData.date}T${spotData.end}`
    ).toISOString();
    axios
      .post("http://88.200.63.148:8102/reservations", {
        u_id: userId,
        start: startTimestamp,
        end: endTimestamp,
        ps_id: spotData.ps_id,
        date: spotData.date,
      })
      .then((response) => {
        alert("Reservation successful!");
        navigator("/myaccount");
      })
      .catch((error) => {
        console.log(error);
      });
    console.log(spotData);
  };

  return (
    <>
      <div className="resContainer">
        <h1>Make a reservation</h1>
        <div className="resBox">
          <form className="row g-3">
            <div className="col-md-6">
              <label htmlFor="inputEmail4" className="form-label">
                Starting time
              </label>
              <input
                type="time"
                className="form-control"
                id="inputEmail4"
                name="start"
                onChange={getText}
              />
            </div>
            <div className="col-md-6">
              <label htmlFor="inputPassword4" className="form-label">
                Ending time
              </label>
              <input
                type="time"
                className="form-control"
                id="inputPassword4"
                name="end"
                onChange={getText}
              />
            </div>
            <div className="col-12">
              <label htmlFor="inputAddress" className="form-label">
                Date of reservation
              </label>
              <input
                type="date"
                className="form-control"
                id="inputAddress"
                placeholder="1234 Main St"
                name="date"
                onChange={getText}
              />
            </div>
            <div className="col-12">
              <label htmlFor="inputAddress2" className="form-label">
                Choose a parking spot
              </label>
              {spots && spots.length > 0 ? (
                <select
                  className="form-select"
                  aria-label="Default select example"
                  onChange={getText}
                  name="ps_id"
                >
                  <option value="" disabled selected>
                    Open this select menu
                  </option>
                  {spots.map((spot) => (
                    <option key={spot.id} value={spot.id}>
                      Koper - {spot.id}
                    </option>
                  ))}
                </select>
              ) : (
                <p>Loading spots...</p>
              )}
            </div>

            <div className="col-12">
              <button
                type="button"
                className="btn btn-primary"
                onClick={reserveSpot}
              >
                Submit
              </button>
            </div>
          </form>
        </div>
      </div>
    </>
  );
}
